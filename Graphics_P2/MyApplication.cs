using System.Diagnostics;
using OpenTK;
using OpenTK.Input;
using System;
using OpenTK.Graphics.OpenGL;

namespace Template
{
	class MyApplication
	{
		// member variables
		public Surface screen;																					// background surface for printing etc.
		Mesh floor, bmw, stoneRoad, tire, tree, gate, woman, moon, sun, skybox, man, lantern1, lantern2;        // a mesh to draw using OpenGL
		const float PI = 3.1415926535f;																			// PI
		float a = 0;																							// car rotation angle
		float c = 0;																							// sun&moon rotation angle
		float b = -.3f * PI;																					// swing rotation angle
		int rainbow = 0;																						// counter for the light colors
		int indRainbow = 0;																						// tracks index of color array
		Vector3[] colors;																						// color array for rainbow colors
		bool positive = true;																					// bool for direction of swing																	
		Stopwatch timer;																						// timer for measuring frame duration
		Shader shader, shaderbox, rshader;																		// shader to use for rendering
		Shader postproc;																						// shader to use for post processing
		Texture grass, stone, wood, red, skin, tiret, silver, moontext, suntext, cubeText;						// texture to use for rendering
		RenderTarget target;																					// intermediate render target
		ScreenQuad quad;																						// screen filling quad for post processing
		bool useRenderTarget = false;
		float cameraRotY = 0;																					// camera rotation around y-axis
		Vector3 cameraTranslation = new Vector3(0f, 0f, 30f);													// camera translation
		SceneGraph scene;																						// scenegraph
		Light[] lights;																							// array of 4 lights
		Node sky;																								// Node for the skybox

		// initialize
		public void Init()
		{
			// load meshes
			floor = new Mesh( "../../assets/floor.obj");
			stoneRoad = new Mesh("../../assets/cobble.obj");
			bmw = new Mesh("../../assets/car.obj");
			tire = new Mesh("../../assets/tire.obj");
			gate = new Mesh("../../assets/torii.obj");
			tree = new Mesh("../../assets/tree41.obj", true);
			woman = new Mesh("../../assets/woman.obj");
			moon = new Mesh("../../assets/moon.obj");
			sun = new Mesh("../../assets/sun.obj");
			skybox = new Mesh("../../assets/box.obj");
			man = new Mesh("../../assets/man.obj");
			lantern1 = new Mesh("../../assets/stone_lantern.obj");
			lantern2 = new Mesh("../../assets/stone_lantern.obj");

			// initialize stopwatch
			timer = new Stopwatch();
			timer.Reset();
			timer.Start();

			// create shaders
			shader = new Shader( "../../shaders/vs.glsl", "../../shaders/fs.glsl" , false);
			postproc = new Shader( "../../shaders/vs_post.glsl", "../../shaders/fs_post.glsl" , false);
			shaderbox = new Shader("../../shaders/skyboxvs.glsl", "../../shaders/skyboxfs.glsl", false);
			rshader = new Shader("../../shaders/reflectvs.glsl", "../../shaders/reflectfs.glsl", false);

			// load textures
			grass = new Texture( "../../assets/grass.jpg" );
			stone = new Texture("../../assets/cobble2.png");
			wood = new Texture("../../assets/bark.jpg");
			red = new Texture("../../assets/red.jpg");
			skin = new Texture("../../assets/skin.jpg");
			tiret = new Texture("../../assets/tire.jpg");
			silver = new Texture("../../assets/darkgrey.jpg");
			moontext = new Texture("../../assets/moontext.jpg");
			suntext = new Texture("../../assets/suntext.jpg");

			//load texture for cubemap
			string[] faces = new string[]
			{
				"../../assets/right.jpg",
				"../../assets/left.jpg",
				"../../assets/top.jpg",
				"../../assets/bottom.jpg",
				"../../assets/front.jpg",
				"../../assets/back.jpg"
			};
			cubeText = new Texture("cubeMap", faces);

			// create the render target
			target = new RenderTarget( screen.width, screen.height );
			quad = new ScreenQuad();

			scene = new SceneGraph();

			//Initialize skybox
			sky = new Node(skybox, cubeText);
			skybox.localTransform(new Matrix4[] { Scale(1f) });
			scene.skybox = sky;

			//Initialize four lights
			lights = new Light[]
			{
				new Light(Vector3.Zero),
				new Light(Vector3.Zero),
				new Light(Vector3.Zero),
				new Light(Vector3.Zero)
			};

			//Initialize rainbow colors
			colors = new Vector3[]
			{
				new Vector3(2550, 0, 0),
				new Vector3(2550, 1270, 0),
				new Vector3(2550, 2550, 0),
				new Vector3(1270, 2550, 0),
				new Vector3(0, 1270, 2550),
				new Vector3(800, 0, 2110),
				new Vector3(2550, 0, 1270)
			};
		}

		// tick for background surface
		public void Tick()
		{
			screen.Clear(0);
			screen.Print("hello world", 2, 2, 0xffff00);
		}

		// tick for OpenGL rendering code
		public void RenderGL()
		{
			// measure frame duration
			float frameDuration = timer.ElapsedMilliseconds;
			timer.Reset();
			timer.Start();

			// prepare matrix for vertex shader
			Matrix4 Tcamera = Matrix4.CreateFromAxisAngle(new Vector3(0, 1, 0), cameraRotY) * Matrix4.CreateTranslation(cameraTranslation) * Matrix4.CreateFromAxisAngle(new Vector3(0, 1, 0), PI);
			Matrix4 Tview = Matrix4.CreatePerspectiveFieldOfView(1.2f, 1.3f, .1f, 1000);

			//Get camera direction
			float xdir = Tview[2, 0]; // x
			float ydir = Tview[2, 1]; // y
			float zdir = Tview[2, 2]; // z

			//Determine the sidevector of the camera
			Vector3 dir = new Vector3(xdir, ydir, zdir);
			Vector3 up = new Vector3(0, 1, 0);
			Vector3 sideVec = Vector3.Normalize(Vector3.Cross(dir, up));

			//Camera position
			Vector4 cameraPos = Tcamera * new Vector4(0, 0, 0, 1) ;
			Vector3 cPos = new Vector3(cameraPos.X, cameraPos.Y, cameraPos.Z);

			//Set lights at moon and sun
			lights[0] = new Light(new Vector3(2000, 1500, 150), new Matrix4[] { Translate(new Vector3(0, -30f, 0)), Matrix4.CreateRotationX(c) }); //moon
			lights[1] = new Light(new Vector3(300, 850, 1650), new Matrix4[] { Translate(new Vector3(0, 30f, 0)), Matrix4.CreateRotationY(a), Matrix4.CreateRotationX(c) }); //sun
			lights[2] = new Light(new Vector3(.9f, .6f, 0f), new Matrix4[] { Translate(new Vector3(-23, 20f, -23))});
			lights[2].setSpotlight(new Vector3(0, -1, 0), (float)Math.Cos(.25f));
			lights[3] = new Light(new Vector3(.9f, .6f, 0f), new Matrix4[] { Translate(new Vector3(-23, 20f, 23)) });
			lights[3].setSpotlight(new Vector3(0, -1, 0), (float)Math.Cos(.25));

			//Initialize scenegraph and node
			scene.root = new Node(floor, grass);
			scene.root.lights = lights;
			scene.root.camera = Tcamera;
			scene.root.view = Tview;
			scene.root.ambient = new Light(new Vector3(0.4f, 0.4f, 0.4f));

			//Check user input
			CheckInput(sideVec);

			//Add nodes to the scenegraph
			CreateSceneGraph(scene);

			// update rotation
			a += 0.0005f * frameDuration;
			c += 0.0001f * frameDuration;
			if ( a > 2 * PI ) a -= 2 * PI;
			if ( c > 2 * PI ) c -= 2 * PI;

			if (b > .3f * PI)
			{
				b -= 0.001f * frameDuration;
				positive = false;
			}
			else if (b < -.3f * PI)
			{
				b += 0.001f * frameDuration;
				positive = true;
			}
			else if (positive)
            {
				b += 0.001f * frameDuration;
			}
            else
            {
				b -= 0.001f * frameDuration;
			}

			//Update counter for lights
			if(rainbow > 1)
            {
				rainbow--;
				if(rainbow % 50 == 0)
                {
					indRainbow++;
                }
				
				for(int i = 0; i < 2; i++)
                {
					lights[i].intensity = colors[indRainbow];
                }
			}

			if ( useRenderTarget )
			{
				// enable render target
				target.Bind();

				// render scene to render target
				sky.RenderBox(shaderbox, Tcamera, Tview);
				scene.Render(shader, rshader, cPos);

				// render quad
				target.Unbind();
				quad.Render( postproc, target.GetTextureID() );
			}
			else
			{
				// render scene directly to the screen

			    sky.RenderBox(shaderbox, Tcamera, Tview);
				scene.Render(shader, rshader, cPos);
			}
		}

		private void CheckInput(Vector3 sidev)
		{
			var keyboard = Keyboard.GetState();
			var mouse = Mouse.GetState();

			//Key left = rotate to the left
			if (keyboard[Key.Left])
			{
				cameraRotY += PI / 50;
			}
			//Key right = rotate to the right
			else if (keyboard[Key.Right])
			{
				cameraRotY -= PI / 50;
			}
			//Key W = go up
			else if (keyboard[Key.W])
			{
				cameraTranslation.Y = cameraTranslation.Y - 1f;
			}
			//Key S = go down
			else if (keyboard[Key.S])
			{
				cameraTranslation.Y = cameraTranslation.Y + 1f;
			}
			//Key A = go left
			else if (keyboard[Key.A])
			{
				cameraTranslation = cameraTranslation - sidev;
			}
			//Key D = go right
			else if (keyboard[Key.D])
			{
				cameraTranslation = cameraTranslation + sidev;
			}
			//Key up = go forward
			else if (keyboard[Key.Up])
			{
				cameraTranslation.Z = cameraTranslation.Z - 1f;
			}
			//Key down = go backwards
			else if (keyboard[Key.Down])
			{
				cameraTranslation.Z = cameraTranslation.Z + 1f;
			}
			//Left click = activate rainbow colors
			else if (mouse[MouseButton.Left])
			{
				rainbow = 350;
				indRainbow = 0;
			}
		}

		private void CreateSceneGraph(SceneGraph scene)
        {
			Node st = scene.root.AddNode(stoneRoad, stone);
			Node tr = scene.root.AddNode(tree, wood);
			Node sw = scene.root.AddNode(tire, tiret);
			Node gt = scene.root.AddNode(gate, red);
			Node mn = scene.root.AddNode(moon, moontext);
			Node sn = scene.root.AddNode(sun, suntext);
			Node car = scene.root.AddNode(bmw, cubeText, true);
			Node l1 = scene.root.AddNode(lantern1, stone);
			Node l2 = scene.root.AddNode(lantern2, stone);
			Node m = car.AddNode(man, skin);
			Node wm = sw.AddNode(woman, skin);

			Vector3 carTVec = new Vector3((float)(2 * Math.Cos(a)), -1.3f, (float)(2 * Math.Sin(a)));

			bmw.localTransform(new Matrix4[] { Scale(4f), Rotate(new Vector3(0, 1, 0), .5f * PI - a), Translate(carTVec) });
			moon.localTransform(new Matrix4[] {Translate(new Vector3(0, 1.5f, 0)), Matrix4.CreateRotationY(a), Matrix4.CreateRotationX(c), Scale(10f) });
			sun.localTransform(new Matrix4[] { Translate(new Vector3(0, -1.5f, 0)), Matrix4.CreateRotationY(a), Matrix4.CreateRotationX(c), Scale(10f) });
			stoneRoad.localTransform(new Matrix4[] { Scale(2f), Rotate(new Vector3(0, 1, 0), .05f * PI), Translate(new Vector3(4.5f, -1.99f, 5f)) });
			tree.localTransform(new Matrix4[] { Scale(.3f), Rotate(new Vector3(1, 0, 0), -.5f * PI), Translate(new Vector3(5f, -3f, -5f)) });
			tire.localTransform(new Matrix4[] { Scale(6f), Translate(new Vector3(0, -1.5f, -1.5f)) * Rotate(new Vector3(1, 0, 0), b), Translate(new Vector3(3.5f, 2.5f, -4f)) });
			woman.localTransform(new Matrix4[] { Scale(.8f), Translate(new Vector3(-.15f, -.1f, .01f)) });
			gate.localTransform(new Matrix4[] { Scale(0.01f), Translate(new Vector3(-1.5f, -2f, 0f)) });
			floor.localTransform(new Matrix4[] { Scale(4f) });
			lantern1.localTransform(new Matrix4[] { Scale(.02f), Translate(new Vector3(-4.75f, -2f, -9.25f)) });
			lantern2.localTransform(new Matrix4[] { Scale(.02f), Translate(new Vector3(-4.75f, -2f, 2.25f)) });
			man.localTransform(new Matrix4[] { Scale(.8f), Translate(new Vector3(.5f,.09f,0)), Rotate(new Vector3(0, 1, 0), -.5f * PI) });
		}

		//Create scaling matrix
		public Matrix4 Scale(float scale)
		{
			Matrix4 sMatrix;
			sMatrix = Matrix4.CreateScale(scale);
			return sMatrix;
		}

		//Create translation matrix
		public Matrix4 Translate(Vector3 tVector)
		{
			Matrix4 tMatrix;
			tMatrix = Matrix4.CreateTranslation(tVector);
			return tMatrix;
		}

		//Create rotation matrix
		public Matrix4 Rotate(Vector3 axis, float angle)
		{
			Matrix4 rMatrix;
			rMatrix = Matrix4.CreateFromAxisAngle(axis, angle);
			return rMatrix;
		}
	}
}