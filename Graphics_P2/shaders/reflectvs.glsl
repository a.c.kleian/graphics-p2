﻿#version 330
 
// shader input
in vec3 vNormal;		// untransformed vertex normal
in vec3 vPosition;		// untransformed vertex position

// shader output
out vec4 normal;		// transformed vertex normal
out vec3 fragPos;		// vertex position worldspace

uniform mat4 transform;
uniform mat4 normalTransform;
 
// vertex shader
void main()
{
	// transform vertex using supplied matrix
	//obtain vposition from stream
	gl_Position = transform * vec4(vPosition, 1.0);

	//transform position to worldspace
	fragPos = vec3(normalTransform * vec4(vPosition, 1.0));

	//obtain vnormal from the stream
	// forward normal and uv coordinate; will be interpolated over triangle
	normal = normalize(normalTransform * vec4( vNormal, 0.0f ));
}