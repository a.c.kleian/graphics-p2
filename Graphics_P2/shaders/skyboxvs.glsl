﻿#version 330
 
// shader input
in vec3 vPosition;		// untransformed vertex position

// shader output
out vec3 texCoords;		//texture coordinates in world space

uniform mat4 transform;
 
// vertex shader
void main()
{
	texCoords = vPosition;
	gl_Position = transform * vec4(vPosition, 1.0);
}