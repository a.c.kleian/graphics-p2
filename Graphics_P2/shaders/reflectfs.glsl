﻿#version 330
 
// shader input
in vec4 normal;			// interpolated normal
in vec3 fragPos;        //fragment position

uniform vec3 viewPos;       //camera position
uniform samplerCube box;    

// shader output
out vec4 outputColor;

// fragment shader
void main()
{
    vec3 I = normalize(fragPos - viewPos);
    vec3 R = reflect(I, normal.xyz);
    outputColor = vec4(texture(box, R).rgb, 1.0);
}