﻿#version 330
 
// shader input
in vec3 texCoords;

uniform samplerCube box;	

// shader output
out vec4 fragCol;

// fragment shader
void main()
{
	fragCol = texture(box, texCoords);
}