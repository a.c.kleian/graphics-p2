﻿#version 330
 
// shader input
in vec2 uv;			        // interpolated texture coordinates
in vec4 normal;			    // interpolated normal
in vec3 fragPos;            // vertex position worldspace

uniform sampler2D pixels;	// texture sampler
uniform vec3 ambientColor;  // ambient light color
uniform vec3 viewPos;       // viewing position
uniform vec3 lCol[4];       // light colors
uniform vec3 lPos[4];       // light positions
uniform vec3 lDir[2];       // light directions
uniform float cutoff[2];    // cutoff angles

// shader output
out vec4 outputColor;

// fragment shader
void main()
{
    vec3 result = vec3(0,0,0);
    vec3 materialCol = texture(pixels, uv).xyz;

    //calculate color by looping over lights
    for(int i = 0; i < 2; i=i+1)
    {     
        vec3 L = lPos[i] - fragPos;
        float dist = length(L);
        L = normalize(L);
        float attenuation = 1.0f / (dist * dist);

        //Diffuse
        float diffuseStrength =  max(0, dot(L, normal.xyz));
        vec3 diffuse = diffuseStrength * materialCol ;

        //Specular
        float specularStrength = 0.5;
        vec3 viewDir = normalize(viewPos - fragPos);
        vec3 reflectDir = reflect(-L, normal.xyz);
        float spec = pow(max(dot(viewDir, reflectDir), 0.0), 32);
        vec3 specular = specularStrength * spec * materialCol;

        vec3 combined = attenuation * lCol[i] * (diffuse + specular); 
        result = result + combined;
    }
       
    //calculate color by looping over spotlights
    for(int i = 0; i < 2; i=i+1)
    {
        vec3 L = normalize(lPos[i + 2] - fragPos);
        float angle = max(dot(L, normalize(-lDir[i])), 0.0);

        if(angle > cutoff[i])
        {
            vec3 combined = lCol[i + 2] * materialCol; 
            result = result + combined;
        }
    }

    //Add ambient color
    vec3 ambient = ambientColor * materialCol;
    result = result + ambient;

    //output color of fragment
    outputColor = vec4(result, 1.0);
}