﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace Template
{
    class SceneGraph
    {
        public Node root;           //root node in the scenegraph
        public Node skybox;         //skybox node 
        public Light[] lights;      //array of lights in the scene
        public Light ambient;       //ambient light in the scene
        public Matrix4 camera;      //camera transform matrix
        public Matrix4 view;        //view transform matrix

        //Render all nodes in the scenegraph
        public void Render(Shader shader, Shader rshader, Vector3 cpos)
        {
            root.RenderNode(Matrix4.Identity, shader, rshader, cpos);
        }
    }

    //Node class for each individual mesh
    class Node : SceneGraph
    {
        private List<Node> children;        //children nodes of the current node
        private Mesh mesh;                  //mesh of the node
        private bool reflect;               //bool indicating whether the node is reflect

        //Constructor
        public Node(Mesh m, Texture t = null, bool r = false)
        {
            mesh = m;
            mesh.texture = t;
            children = new List<Node>();
            reflect = r;
        }

        //Add child node
        public Node AddNode(Mesh child, Texture t = null, bool r = false)
        {
            Node c = new Node(child, t, r);
            c.lights = lights;
            c.ambient = ambient;
            c.camera = camera;
            c.view = view;
            children.Add(c);
            return c;
        }

        //takes a camera matrix
        //renders all meshes in hierarchy
        //to determine final transform, matrix concatenation should be used
        //to combine all matrices starting with camera matrix down to each individual mesh
        public void RenderNode(Matrix4 transform, Shader shader, Shader rshader, Vector3 cPos)
        {
            Matrix4 ntf = mesh.modelView * transform;
            Matrix4 tf = mesh.modelView * transform * camera * view;

            //render mesh
            if (reflect)
            {
                mesh.RenderReflect(rshader, ntf, tf, cPos);
            }
            else
            {
                mesh.Render(shader, ntf, tf, ambient, lights, cPos);
            }

            //render children nodes recursively
            for (int i = 0; i < children.Count; i++)
            {
                children[i].RenderNode(mesh.modelView * transform, shader, rshader, cPos);
            }
        }

        //Render skybox
        public void RenderBox(Shader shader, Matrix4 camera, Matrix4 view)
        {
            Matrix3 c3 = new Matrix3(camera);
            Matrix4 c4 = new Matrix4(c3);

            Matrix4 ntf = mesh.modelView * c4 * view;
            mesh.RenderBox(shader, ntf);
        }

    }
}
