﻿using System;
using System.Runtime.InteropServices;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace Template
{
	// mesh and loader based on work by JTalton; http://www.opentk.com/node/642

	public class Mesh
	{
		// data members
		public ObjVertex[] vertices;            // vertex positions, model space
		public ObjTriangle[] triangles;         // triangles (3 vertex indices)
		public ObjQuad[] quads;                 // quads (4 vertex indices)
		int vertexBufferId;                     // vertex buffer
		int triangleBufferId;                   // triangle buffer
		int quadBufferId;                       // quad buffer
		public Matrix4 modelView;				//Local transform 
		public Texture texture;					//Texture of mesh

		// constructor
		public Mesh( string fileName, bool noNormals = false)
		{
			MeshLoader loader = new MeshLoader();
			loader.Load( this, fileName, noNormals );
		}

		// initialization; called during first render
		public void Prepare( Shader shader )
		{
			if( vertexBufferId == 0 )
			{
				// generate interleaved vertex data (uv/normal/position (total 8 floats) per vertex)
				GL.GenBuffers( 1, out vertexBufferId );
				GL.BindBuffer( BufferTarget.ArrayBuffer, vertexBufferId );
				GL.BufferData( BufferTarget.ArrayBuffer, (IntPtr)(vertices.Length * Marshal.SizeOf( typeof( ObjVertex ) )), vertices, BufferUsageHint.StaticDraw );

				// generate triangle index array
				GL.GenBuffers( 1, out triangleBufferId );
				GL.BindBuffer( BufferTarget.ElementArrayBuffer, triangleBufferId );
				GL.BufferData( BufferTarget.ElementArrayBuffer, (IntPtr)(triangles.Length * Marshal.SizeOf( typeof( ObjTriangle ) )), triangles, BufferUsageHint.StaticDraw );

				// generate quad index array
				GL.GenBuffers( 1, out quadBufferId );
				GL.BindBuffer( BufferTarget.ElementArrayBuffer, quadBufferId );
				GL.BufferData( BufferTarget.ElementArrayBuffer, (IntPtr)(quads.Length * Marshal.SizeOf( typeof( ObjQuad ) )), quads, BufferUsageHint.StaticDraw );
			}
		}

		// render the mesh using the supplied shader and matrix
		public void Render( Shader shader, Matrix4 normalTransform, Matrix4 transform, Light ambient, Light[] lights, Vector3 cameraPos)
		{
			// on first run, prepare buffers
			Prepare( shader );

			// safety dance
			GL.PushClientAttrib( ClientAttribMask.ClientVertexArrayBit );

			if (texture != null)
			{
				// enable texture
				int texLoc = GL.GetUniformLocation(shader.programID, "pixels");
				GL.Uniform1(texLoc, 0);
				GL.ActiveTexture(TextureUnit.Texture0);
				GL.BindTexture(TextureTarget.Texture2D, texture.id);
			}

			// enable shader
			GL.UseProgram( shader.programID );

			// pass transform to vertex shader
			GL.UniformMatrix4( shader.uniform_mview, false, ref transform );
			GL.UniformMatrix4( shader.uniform_model, false, ref normalTransform);
			GL.Uniform3(shader.uniform_AmbCol, ref ambient.intensity);
			GL.Uniform3(shader.uniform_viewPos, ref cameraPos);

			//Pass lights to the shader
			for (int i = 0; i < 4; i++)
            {
				GL.Uniform3(shader.attribute_lPos[i], ref lights[i].location);
				GL.Uniform3(shader.uniform_lCol[i], ref lights[i].intensity);
			}
			for (int i = 0; i < 2; i++)
			{
				GL.Uniform3(shader.uniform_lDir[i], ref lights[i + 2].direction);
				GL.Uniform1(shader.uniform_cutoff[i], lights[i + 2].cutoff);
			}

			// enable position, normal and uv attributes
			GL.EnableVertexAttribArray( shader.attribute_vpos );
			GL.EnableVertexAttribArray( shader.attribute_vnrm );
			GL.EnableVertexAttribArray( shader.attribute_vuvs );

			// bind interleaved vertex data
			GL.EnableClientState( ArrayCap.VertexArray );
			GL.BindBuffer( BufferTarget.ArrayBuffer, vertexBufferId );
			GL.InterleavedArrays( InterleavedArrayFormat.T2fN3fV3f, Marshal.SizeOf( typeof( ObjVertex ) ), IntPtr.Zero );

			// link vertex attributes to shader parameters 
			GL.VertexAttribPointer( shader.attribute_vuvs, 2, VertexAttribPointerType.Float, false, 32, 0 );
			GL.VertexAttribPointer( shader.attribute_vnrm, 3, VertexAttribPointerType.Float, true, 32, 2 * 4 );
			GL.VertexAttribPointer( shader.attribute_vpos, 3, VertexAttribPointerType.Float, false, 32, 5 * 4 );

			// bind triangle index data and render
			GL.BindBuffer( BufferTarget.ElementArrayBuffer, triangleBufferId );
			GL.DrawArrays( PrimitiveType.Triangles, 0, triangles.Length * 3 );

			// bind quad index data and render
			if( quads.Length > 0 )
			{
				GL.BindBuffer( BufferTarget.ElementArrayBuffer, quadBufferId );
				GL.DrawArrays( PrimitiveType.Quads, 0, quads.Length * 4 );
			}
			
			//Restore to deafault texture
			GL.BindTexture(TextureTarget.Texture2D, 0);

			// restore previous OpenGL state
			GL.UseProgram( 0 );
			GL.PopClientAttrib();
		}

		public void RenderReflect(Shader shader, Matrix4 normalTransform, Matrix4 transform, Vector3 cameraPos)
		{
			// on first run, prepare buffers
			Prepare(shader);

			// safety dance
			GL.PushClientAttrib(ClientAttribMask.ClientVertexArrayBit);

			if (texture != null)
			{
				// enable texture
				int texLoc = GL.GetUniformLocation(shader.programID, "box");
				GL.Uniform1(texLoc, 0);
				GL.ActiveTexture(TextureUnit.Texture0);
				GL.BindTexture(TextureTarget.TextureCubeMap, texture.id);
			}

			// enable shader
			GL.UseProgram(shader.programID);

			// pass transform to vertex shader
			GL.UniformMatrix4(shader.uniform_mview, false, ref transform);
			GL.UniformMatrix4(shader.uniform_model, false, ref normalTransform);
			GL.Uniform3(shader.uniform_viewPos, ref cameraPos);

			// enable position, normal and uv attributes
			GL.EnableVertexAttribArray(shader.attribute_vpos);
			GL.EnableVertexAttribArray(shader.attribute_vnrm);

			// bind interleaved vertex data
			GL.EnableClientState(ArrayCap.VertexArray);
			GL.BindBuffer(BufferTarget.ArrayBuffer, vertexBufferId);
			GL.InterleavedArrays(InterleavedArrayFormat.T2fN3fV3f, Marshal.SizeOf(typeof(ObjVertex)), IntPtr.Zero);

			// link vertex attributes to shader parameters 
			GL.VertexAttribPointer(shader.attribute_vnrm, 3, VertexAttribPointerType.Float, true, 32, 2 * 4);
			GL.VertexAttribPointer(shader.attribute_vpos, 3, VertexAttribPointerType.Float, false, 32, 5 * 4);

			// bind triangle index data and render
			GL.BindBuffer(BufferTarget.ElementArrayBuffer, triangleBufferId);
			GL.DrawArrays(PrimitiveType.Triangles, 0, triangles.Length * 3);

			// bind quad index data and render
			if (quads.Length > 0)
			{
				GL.BindBuffer(BufferTarget.ElementArrayBuffer, quadBufferId);
				GL.DrawArrays(PrimitiveType.Quads, 0, quads.Length * 4);
			}

			//Restore to deafault texture
			GL.BindTexture(TextureTarget.Texture2D, 0);

			// restore previous OpenGL state
			GL.UseProgram(0);
			GL.PopClientAttrib();
		}

		public void RenderBox(Shader shader, Matrix4 transform)
		{
			GL.DepthMask(false);

			// on first run, prepare buffers
			Prepare(shader);

			// safety dance
			GL.PushClientAttrib(ClientAttribMask.ClientVertexArrayBit);

			if (texture != null)
			{
				// enable texture
				int texLoc = GL.GetUniformLocation(shader.programID, "box");
				GL.Uniform1(texLoc, 0);
				GL.ActiveTexture(TextureUnit.Texture0);
				GL.BindTexture(TextureTarget.TextureCubeMap, texture.id);
			}

			// enable shader
			GL.UseProgram(shader.programID);

			// pass transform to vertex shader
			GL.UniformMatrix4(shader.uniform_mview, false, ref transform);

			// enable position attribute
			GL.EnableVertexAttribArray(shader.attribute_vpos);

			// bind interleaved vertex data
			GL.EnableClientState(ArrayCap.VertexArray);
			GL.BindBuffer(BufferTarget.ArrayBuffer, vertexBufferId);
			GL.InterleavedArrays(InterleavedArrayFormat.T2fN3fV3f, Marshal.SizeOf(typeof(ObjVertex)), IntPtr.Zero);

			// link vertex attributes to shader parameters 
			GL.VertexAttribPointer(shader.attribute_vpos, 3, VertexAttribPointerType.Float, false, 32, 5 * 4);

			// bind triangle index data and render
			GL.BindBuffer(BufferTarget.ElementArrayBuffer, triangleBufferId);
			GL.DrawArrays(PrimitiveType.Triangles, 0, triangles.Length * 3);

			// bind quad index data and render
			if (quads.Length > 0)
			{
				GL.BindBuffer(BufferTarget.ElementArrayBuffer, quadBufferId);
				GL.DrawArrays(PrimitiveType.Quads, 0, quads.Length * 4);
			}

			//Restore to deafault texture
			GL.BindTexture(TextureTarget.Texture2D, 0);

			GL.DepthMask(true);

			// restore previous OpenGL state
			GL.UseProgram(0);
			GL.PopClientAttrib();
		}

		//local transform matrix for the mesh
		//concatenate matrices to get desired transform 
		//order srt = scale, rotate, translate
		public void localTransform(Matrix4[] T)
        {
			Matrix4 srtMatrix = T[T.Length - 1];

			for(int i = T.Length - 2; i >= 0; i--)
            {
				srtMatrix = T[i] * srtMatrix;
			}
			modelView = srtMatrix;
		}


		//rotate around fixed point by translating to origin, rotating and translating back

		// layout of a single vertex
		[StructLayout( LayoutKind.Sequential )]
		public struct ObjVertex
		{
			public Vector2 TexCoord;
			public Vector3 Normal;
			public Vector3 Vertex;
		}

		// layout of a single triangle
		[StructLayout( LayoutKind.Sequential )]
		public struct ObjTriangle
		{
			public int Index0, Index1, Index2;
		}

		// layout of a single quad
		[StructLayout( LayoutKind.Sequential )]
		public struct ObjQuad
		{
			public int Index0, Index1, Index2, Index3;
		}
	}
}