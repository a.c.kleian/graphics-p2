﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace Template
{
    public class Light
    {
        public Vector3 location;        // location of light
        public Vector3 intensity;       // light intensity for red,green,blue
        public Vector3 direction;       // spotlight direction
        public float cutoff;            // cutoff angle of spotlight

        //Constructor
        public Light(Vector3 i, Matrix4[] T = null)
        {
            Matrix4 acc = Matrix4.Identity;

            //Determine light location with transformation matrix
            if (T != null)
            {
                for (int x = T.Length - 1; x >= 0; x--)
                {
                    acc = T[x] * acc;
                }
            }
            location = new Vector3(acc.Row3);
            intensity = i;
        }

        //Set spotlight direction and cutoff angle
        public void setSpotlight(Vector3 dir, float angleCut)
        {
            direction = dir;
            cutoff = angleCut;
        }
    }
}
