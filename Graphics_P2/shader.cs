﻿using System;
using System.IO;
using OpenTK.Graphics.OpenGL;

namespace Template
{
	public class Shader
	{
		// data members
		public int programID, vsID, fsID;
		public int attribute_vpos;
		public int attribute_vnrm;
		public int attribute_vuvs;
		public int uniform_mview;
		public int uniform_model;
		public int uniform_AmbCol;		// ambient light color
		public int uniform_viewPos;		// camera position
		public int[] attribute_lPos;	// array of light positions
		public int[] uniform_lCol;		// array of light colors
		public int[] uniform_lDir;		// array of light directions
		public int[] uniform_cutoff;	// array of cutoff angles

		// constructor
		public Shader(String vertexShader, String fragmentShader, bool skybox)
		{
			if (!skybox)
			{
				// compile shaders
				programID = GL.CreateProgram();
				Load(vertexShader, ShaderType.VertexShader, programID, out vsID);
				Load(fragmentShader, ShaderType.FragmentShader, programID, out fsID);
				GL.LinkProgram(programID);
				Console.WriteLine(GL.GetProgramInfoLog(programID));

				// get locations of shader parameters
				attribute_vpos = GL.GetAttribLocation(programID, "vPosition");
				attribute_vnrm = GL.GetAttribLocation(programID, "vNormal");
				attribute_vuvs = GL.GetAttribLocation(programID, "vUV");
				uniform_mview = GL.GetUniformLocation(programID, "transform");
				uniform_model = GL.GetUniformLocation(programID, "normalTransform");
				uniform_AmbCol = GL.GetUniformLocation(programID, "ambientColor");
				uniform_viewPos = GL.GetUniformLocation(programID, "viewPos");

				//Get location of position, color, direction and cutoff parameters of lights
				attribute_lPos = new int[4];
				uniform_lCol = new int[4];
				uniform_lDir = new int[2];
				uniform_cutoff = new int[2];

				for (int i = 0; i < 4; i++)
				{
					attribute_lPos[i] = GL.GetUniformLocation(programID, "lPos[" + i + "]");
					uniform_lCol[i] = GL.GetUniformLocation(programID, "lCol[" + i + "]");
				}

				for (int i = 0; i < 2; i++)
				{
					uniform_lDir[i] = GL.GetUniformLocation(programID, "lDir[" + i + "]");
					uniform_cutoff[i] = GL.GetUniformLocation(programID, "cutoff[" + i + "]");
				}
			}
            else
            {
				// compile shaders
				programID = GL.CreateProgram();
				Load(vertexShader, ShaderType.VertexShader, programID, out vsID);
				Load(fragmentShader, ShaderType.FragmentShader, programID, out fsID);
				GL.LinkProgram(programID);
				Console.WriteLine(GL.GetProgramInfoLog(programID));

				// get locations of shader parameters
				attribute_vpos = GL.GetAttribLocation(programID, "vPosition");
				attribute_vnrm = GL.GetAttribLocation(programID, "vNormal");
				uniform_mview = GL.GetUniformLocation(programID, "transform");
				uniform_model = GL.GetUniformLocation(programID, "normalTransform");
				uniform_viewPos = GL.GetUniformLocation(programID, "viewPos");
			}
		}

		// loading shaders
		void Load( String filename, ShaderType type, int program, out int ID )
		{
			// source: http://neokabuto.blogspot.nl/2013/03/opentk-tutorial-2-drawing-triangle.html
			ID = GL.CreateShader( type );
			using( StreamReader sr = new StreamReader( filename ) ) GL.ShaderSource( ID, sr.ReadToEnd() );
			GL.CompileShader( ID );
			GL.AttachShader( program, ID );
			Console.WriteLine( GL.GetShaderInfoLog( ID ) );
		}
	}
}
