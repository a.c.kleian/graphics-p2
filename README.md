Aielynn Kleian 6441130, Odette Fischer 6254861

# Demo #

Running the program will start off with the camera in the centre of the scene: 
a playground for nostalgic adults, a down the memory lane retreat of sorts. 
The scene contains a woman on a tire swing and a man in a "driving" car, accompanied by a torii, a sun & moon and a stunning view from above a lake 
surrounded by mountains. Using the operation keys specified down below, the viewer can take a look through the scene. 
With a left mouse click, rainbow lights which follow the sun and moon are activated. 
The spotlights emphasize the two stone lanterns on each side of the torii. 


# Architercture #

[Datastructures]

In this rasterizer, a datastructure Node is created to use for each individual mesh. Each Node has a
list of its children nodes (if it has any), its mesh, and a bool for whether the texture of the mesh is reflective (currently only the car surface).
It also contains a method RenderNode, in which the children nodes of a Node is rendered recursively. The parameters of this method
pass the transformation matrix that is created by the transformations of the parents of the node (with respect to the camera and the view matrices).

The SceneGraph class has member variables for the root node in the scenegraph as well as the lights, 
and contains a Render method in which renders all the nodes in the scenegraph starting from the root node, by calling
the RenderNode method for the root node.

The mesh class was expanded by adding a methods for reflectivity and cubemapping in order to pass information
to the vertex and fragment shaders that were created for each of them. A method for the local transform for a mesh was also added.

[Application engineering]

The application class contains methods to operate transformations on matrices.
The initializing method sets the meshes and their textures (including the skybox) as well as the array of Light objects. 
In the RenderGL method, meshes are added to the scenegraph, with the floor as the root node (along with the lights). 
Different angles that are updated in each tick are member variables of the application class, 
used for the animation of the tire swing, car and sun & moon.
With a CheckInput method, the camera position can be changed at run-time by the user's input. 


[Lights]

The Phong shading model (with an exponent of 32) is implemented in the fragment shader that was provided with the template,
using uniform variables to pass the lights' position, color, and direction.
A Light datastructure was created from which a simple Light object can be made, and this can also be changed into a spotlight.
The fragment shader is changed so that it can handle multiple lights; the loops used currently can be changed 
to handle an arbitrary number of lights in the scene.


**Note** The used tree model didn't include any normals, thus the meshloader was changed in a way such that it can to create normals 
if a model doesn't have any by default.


# Moving camera #

Operating keys:
- Forwards (z-axis): arrow up key
- Backwards (z-axis): arrow down key
- Clockwise rotation (y-axis): arrow left key
- Anti-clockwise rotation (y-axis): arrow right key
- Move to the left (x-axis): letter A key
- Move to the right (x-axis): letter D key
- Up (on y-axis): letter W key
- Down (on y-axis): letter S key



# Implemented extra's #

* (Interactive) Lights
A total of 4 lights are set in the scene:
	* Two interactive lights with changing rainbow colors
	- Application class contains three member variables for the rainbow colors:
		- Array of rainbow colors
		- int value for index of rainbow array, which changes using:
			- int value as a counter for when to increase the index
	- With a left mouse click on anything in the scene, the two rainbow lights - each following either the sun or moon - are turned on: 
		~ After all colours (of the rainbow) have been shown, it returns back to the default state.
	- Right mouse click deactivates rainbow lights (before they ended by themselves).

	* Spotlights
	- Two spotlights put the two stone lanterns on display.
		- setSpotlight method in Light class to define spotlight direction and cutoff angle (set at cosine(1/4))

* Cube mapping (skybox)
 	- Skybox class to store the vertices of the skybox
	- Use of cube obj for the mesh
	- Renderbox method in mesh class passing values to vertex and fragment shaders made for the skybox
	- Renderbox method in Node datastructure to call Renderbox method in mesh class for skybox node

* Vignetting and chromatic aberration
useRenderTarget bool = true by default, set to false to turn off these effects.
Post-processing fragment shader was changed to add these features:
	- Vignetting
	Change the brightness near the edges of the frame.
		~ float variable to determine extent of vignetting effect
		~ smoothstep function for smooth
	- (Transverse) Chromatic Aberration
	Lens cannot focus on all colors to the same focal point.
		~ Distortion method
	 	~ Offset method

# Sources #

* Vignetting: https://github.com/spite/Wagner/blob/master/fragment-shaders/vignette-fs.glsl 
* Chromatic aberration: https://github.com/spite/Wagner/blob/master/fragment-shaders/chromatic-aberration-fs.glsl
https://en.wikipedia.org/wiki/Chromatic_aberration
* Phong shading model: https://opentk.net/learn/chapter2/2-basic-lighting.html
* Cubemapping: https://learnopengl.com/Advanced-OpenGL/Cubemaps
* (Spot)lights: https://opentk.net/learn/chapter2/5-light-casters.html
* 3D models:
- Sketchfab: tree, stone lanterns, torii (gate), stones (on the floor)
- Paint 3D (3D library): man, woman, tire, sun, moon
- Textures: https://www.cgtrader.com/free-3d-models and google images 
